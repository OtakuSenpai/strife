package com.serebit.strife.internal.packets

internal interface EntityPacket {
    val id: Long
}
