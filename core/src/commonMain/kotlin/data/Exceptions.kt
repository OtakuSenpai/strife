package com.serebit.strife.data

class UnknownTypeCodeException(message: String) : IllegalArgumentException(message)

class UnknownEntityTypeException(message: String) : IllegalArgumentException(message)

class UnknownOpcodeException(message: String) : IllegalArgumentException(message)
